import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import { headers } from './utilities/headers';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

polka()
	.use(
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		headers,
		sapper.middleware({
			session: (req, res) => ({
				config: {
					baseUrl: process.env.BASEURL || '//' + req.headers.host,
					backendUrl: process.env.BACKENDURL,
				}
			})
		}),
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
