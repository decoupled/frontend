import fetch from 'node-fetch';
import cache from '../../utilities/api/cache';
import proxy from '../../utilities/api/proxy';
import { directive } from "../../utilities/headers";

export async function get(req, res, next) {
    try {
        const path = req.url.replace('/api', '').replace('.json', '');
        const json = cache.get(path);
        if (json) {
            respond(res, 200, json, {
                'Content-Type': 'application/json',
                'Cache-Control': directive
            });
        } else {
            const response = await proxy.get(req, req.params);
            if (response.ok) {
                const json = await response.json();
                cache.set(path, json);
                respond(res, 200, json, {
                    'Content-Type': 'application/json',
                    'Cache-Control': directive
                });
            } else {
                respond(res, 404, {
                    message: 'Not found',
                }, {
                    'Content-Type': 'application/json',
                });
            }
        }
    } catch (e) {
        respond(res, 500, {
            error: e,
        },{
            'Content-Type': 'application/json',
        });
    }
}

function respond(res, code, content, headers = {}) {
    res.statusCode = code;
    Object.keys(headers).forEach((name) => {
        res.setHeader(name, headers[name]);
    });
    return res.end(JSON.stringify(content));
}
