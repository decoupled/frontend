import LRU from 'lru-cache';

let cache;
if (process.__CACHE__) {
    cache = process.__CACHE__;
} else {
    cache = new LRU({ maxAge: 1000 * (process.env.CACHE_TTL ? Number(process.env.CACHE_TTL) : 1 ) });
    process.__CACHE__ = cache;
}

export default {
    get: (path) => {
        return cache.get(path);
    },
    set: (path, item) => {
        cache.set(path, item);
    }
};
