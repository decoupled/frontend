import fetch from "node-fetch";

export default {
    get: async function (req, {path}) {
        try {
            let url;
            if (path[0] === 'menu' /*.includes('/api/menu/')*/) {
                url = process.env.BACKENDURL + '/api/menu_items/' + path.splice(1).join('/') + '?_format=json';
            } else if (path.length === 1 && path[0] === 'sitemap') {
                url = process.env.BACKENDURL + '/api/' + path.join('/') + '?_format=json';
            } else {
                if (path.length === 1 && path[0] === 'index') path[0] = '';
                url = process.env.BACKENDURL + '/api/' + path.join('/') + '?_format=api_json';
            }
            const response = await fetch(url);
            return response;
        } catch {
            throw e;
        }
    },
};
