export async function get(path, session, context) {
    try {
        let cache;
        if (!process.browser) {
            cache = (await import('./cache')).default;
            const cached = cache.get(path);
            if (cached) return cached;
        }
        const response = await context.fetch((!process.browser && session.config.baseUrl ? session.config.baseUrl + '/' : '') + 'api' + path + '.json');
        if (response.ok) {
            const data = await response.json();
            if (!process.browser) cache.set(path, data);
            return data;
        } else {
            const error = new Error(response.statusText)
            error.status = response.status;
            throw error;
        }
    } catch {
        throw e;
    }
}
