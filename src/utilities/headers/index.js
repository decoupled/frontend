export const directive = 'public, max-age=60, s-maxage=3600, stale-if-error=31536000, stale-while-revalidate=31536000';

export const headers = (req, res, next) => {
    const origSetHeader = res.setHeader
    res.setHeader = function (key, value) {
        if (key === 'Cache-Control') {
            if (value === 'max-age=31536000, immutable') { // Static files
                return origSetHeader.apply(this, ['Cache-Control', 'public, max-age=31536000, stale-if-error=31536000, stale-while-revalidate=31536000, immutable']);
            }
            if (value === 'max-age=600') { // HTML files
                return origSetHeader.apply(this, ['Cache-Control', directive]);
            }
        }

        return origSetHeader.apply(this, arguments);
    }
    return next();
};
