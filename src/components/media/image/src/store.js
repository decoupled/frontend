import { writable } from 'svelte/store';

export let cache;

if (process.browser) {
    cache = writable(localStorage.getItem("images") || []);
} else {
    cache = writable([]);
}
